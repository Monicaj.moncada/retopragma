package com.ejercicio.pragma.persistence.crud.Mongo;

import com.ejercicio.pragma.domain.dto.mongodb.ImaPersonMongoDtoDb;
import com.ejercicio.pragma.persistence.entity.MongoDoc.ImgPersonDocMongoDB;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface ImgPersonCrudMongoDB extends MongoRepository<ImgPersonDocMongoDB, String> {

    @Query("{nameImgD:'?0'}")
    ImaPersonMongoDtoDb findItemByNameImgD(String nameImag);
    @Query("{idenImgPerson:'?0'}")
    List<ImgPersonDocMongoDB>  findItemByIdenImgPerson(int identificacion);

}

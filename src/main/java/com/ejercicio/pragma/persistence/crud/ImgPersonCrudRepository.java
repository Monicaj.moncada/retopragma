package com.ejercicio.pragma.persistence.crud;


import com.ejercicio.pragma.persistence.entity.ImgPersona;
import org.springframework.data.repository.CrudRepository;

public interface ImgPersonCrudRepository extends CrudRepository<ImgPersona, Integer> {
}

package com.ejercicio.pragma.persistence.crud;

import com.ejercicio.pragma.persistence.entity.Persona;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PersonaCrudRepository extends CrudRepository<Persona, Integer> {

    //Query methods
    List<Persona> findByNroIdentificacion(int nroIdentificacion);

    Optional<List<Persona>> findByEdadGreaterThanEqual(int edad);

    Optional<List<Persona>> findByEdadLessThanEqual(int edad);

}

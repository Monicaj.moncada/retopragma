package com.ejercicio.pragma.persistence.mapper.Mongo;


import com.ejercicio.pragma.domain.dto.mongodb.ImaPersonMongoDtoDb;
import com.ejercicio.pragma.persistence.entity.MongoDoc.ImgPersonDocMongoDB;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ImgPersonMapperMongoDB {
    @Mappings(value = {
            @Mapping(source = "idImg",target = "idD"),
            @Mapping(source = "nameImg",target = "nameImgD"),
            @Mapping(source = "idenImgPerson",target = "idenImgPersonD")
    })

    ImaPersonMongoDtoDb toImagen(ImgPersonDocMongoDB imgPerson);
    List<ImaPersonMongoDtoDb> toImgs(List<ImgPersonDocMongoDB> imgPers);
    @InheritInverseConfiguration
    ImgPersonDocMongoDB toImgPerson(ImaPersonMongoDtoDb imgDto);
}

package com.ejercicio.pragma.persistence.mapper;

import com.ejercicio.pragma.domain.dto.PersonDto;
import com.ejercicio.pragma.persistence.entity.Persona;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {ImgPersonMapper.class})
public interface PersonaMapper {
    @Mappings(value = {
            @Mapping(source = "idPersona", target = "idPerson"),
            @Mapping(source = "nombre", target = "nombreP"),
            @Mapping(source = "apellido", target = "apellidoP"),
            @Mapping(source = "tipoId", target = "tipoIdP"),
            @Mapping(source = "nroIdentificacion", target = "nroIdentificacionP"),
            @Mapping(source = "edad", target = "edadP"),
            @Mapping(source = "ciudad", target = "ciudadP"),
            @Mapping(source = "idImg", target = "idImgP")
    })
    PersonDto toPerson(Persona persona);

    List<PersonDto> toPersons (List<Persona> personas);

    @InheritInverseConfiguration
    @Mapping(target = "imgsPersona", ignore = true)
    Persona toPersona(PersonDto personDto);
}

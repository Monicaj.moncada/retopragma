package com.ejercicio.pragma.persistence.mapper;

import com.ejercicio.pragma.domain.dto.ImgPersonDto;
import com.ejercicio.pragma.persistence.entity.ImgPersona;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = {PersonaMapper.class})
public interface ImgPersonMapper {
    @Mappings({
            @Mapping(source = "idImgPersona", target = "idImgPerson"),
            @Mapping(source = "nomImgPersona", target = "nomImagPerson"),
            @Mapping(source = "idenImgPersona", target = "idenImgPerson")
    })

    ImgPersonDto toImgPerson(ImgPersona img);

    @InheritInverseConfiguration
    @Mapping(target = "imgPersona", ignore = true)
    ImgPersona toImgPersona(ImgPersonDto imgPerson);
}

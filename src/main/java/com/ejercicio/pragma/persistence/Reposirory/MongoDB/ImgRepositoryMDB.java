package com.ejercicio.pragma.persistence.Reposirory.MongoDB;

import com.ejercicio.pragma.domain.dto.mongodb.ImaPersonMongoDtoDb;
import com.ejercicio.pragma.domain.repository.Mongo.ImgPersonRepositoryMongoDB;
import com.ejercicio.pragma.persistence.crud.Mongo.ImgPersonCrudMongoDB;
import com.ejercicio.pragma.persistence.entity.MongoDoc.ImgPersonDocMongoDB;
import com.ejercicio.pragma.persistence.mapper.Mongo.ImgPersonMapperMongoDB;
import com.mongodb.BasicDBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ImgRepositoryMDB implements ImgPersonRepositoryMongoDB {

    @Autowired
    ImgPersonCrudMongoDB crudImgPersonCrudMongoDB;
    @Autowired
    private ImgPersonMapperMongoDB imgPersonMapperMongoDB;

    @Override
    public List<ImaPersonMongoDtoDb> getAllImg(){
        List<ImgPersonDocMongoDB> listImg = crudImgPersonCrudMongoDB.findAll();
        return imgPersonMapperMongoDB.toImgs(listImg);
    }

    @Override
    public List<ImaPersonMongoDtoDb> consultarImagenPorIdenti(int identificacion) {
        List<ImgPersonDocMongoDB> imagenes =(List<ImgPersonDocMongoDB> ) crudImgPersonCrudMongoDB.findItemByIdenImgPerson(identificacion);
        return imgPersonMapperMongoDB.toImgs(imagenes);
    }

    @Override
    public void delete(int idenPersona) {
        crudImgPersonCrudMongoDB.deleteById(String.valueOf(idenPersona));

    }

    @Override
    public ImaPersonMongoDtoDb save(ImaPersonMongoDtoDb imgMongo){
        ImgPersonDocMongoDB img = imgPersonMapperMongoDB.toImgPerson(imgMongo);
        return imgPersonMapperMongoDB.toImagen(crudImgPersonCrudMongoDB.save(img));
    }

}

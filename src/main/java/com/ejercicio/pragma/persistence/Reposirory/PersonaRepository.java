package com.ejercicio.pragma.persistence.Reposirory;

import com.ejercicio.pragma.domain.dto.PersonDto;
import com.ejercicio.pragma.domain.repository.PersonRepository;
import com.ejercicio.pragma.persistence.crud.ImgPersonCrudRepository;
import com.ejercicio.pragma.persistence.crud.PersonaCrudRepository;
import com.ejercicio.pragma.persistence.entity.ImgPersona;
import com.ejercicio.pragma.persistence.entity.Persona;
import com.ejercicio.pragma.persistence.mapper.ImgPersonMapper;
import com.ejercicio.pragma.persistence.mapper.PersonaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PersonaRepository implements PersonRepository {
    @Autowired
    private PersonaCrudRepository personaCrudRepository;
    @Autowired
    private PersonaMapper mapper;

    @Autowired
    private ImgPersonCrudRepository imgPersonCrudRepository;

    @Autowired
    private ImgPersonMapper mapperImg;

    @Override
    public List<PersonDto> getAll(){
        List<Persona> personas = (List<Persona>) personaCrudRepository.findAll();
        return mapper.toPersons(personas);
    }

    @Override
    public List<PersonDto> getByPerson(int idPersona){
        List<Persona> personas = (List<Persona>) personaCrudRepository.findByNroIdentificacion(idPersona);
        return mapper.toPersons(personas);
    }

    @Override
    public Optional<PersonDto> getPersona(int idPersona){
        return personaCrudRepository.findById(idPersona).map(per -> mapper.toPerson(per));
    }

    @Override
    public PersonDto save(PersonDto pers) {

        ImgPersona imgpers = new ImgPersona();
        imgpers.setNomImgPersona(pers.getIdImgP());
        imgpers.setIdenImgPersona(pers.getNroIdentificacionP());
        imgPersonCrudRepository.save(imgpers);
        pers.setIdImgP(String.valueOf(imgpers.getIdImgPersona()));
        Persona ps = mapper.toPersona(pers);
        return mapper.toPerson(personaCrudRepository.save(ps));

    }

    @Override
    public Optional<List<PersonDto>> getAgePersona(int edadPersona){
        Optional<List<Persona>> persona = personaCrudRepository.findByEdadGreaterThanEqual(edadPersona);
       return persona.map(pers -> mapper.toPersons(pers));
    }
    @Override
    public Optional<List<PersonDto>> getAgePersonaMenorIgual(int edadPersona){
        Optional<List<Persona>> persona = personaCrudRepository.findByEdadLessThanEqual(edadPersona);
        return persona.map(perso -> mapper.toPersons(perso));
    }


    @Override
    public void delete(int idPersona){
       personaCrudRepository.deleteById(idPersona);
    }

}

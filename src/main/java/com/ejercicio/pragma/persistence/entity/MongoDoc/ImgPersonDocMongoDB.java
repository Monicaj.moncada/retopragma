package com.ejercicio.pragma.persistence.entity.MongoDoc;
import org.bson.types.Binary;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "ImgPersonaMDB")
public class ImgPersonDocMongoDB {
    @Id
    private String idImg;
    private Binary nameImg;
    private Integer idenImgPerson;

    public String getIdImg() {
        return idImg;
    }

    public void setIdImg(String idImg) {
        this.idImg = idImg;
    }

    public Binary getNameImg() {
        return nameImg;
    }

    public void setNameImg(Binary nameImg) {
        this.nameImg = nameImg;
    }

    public Integer getIdenImgPerson() {
        return idenImgPerson;
    }

    public void setIdenImgPerson(Integer idenImgPerson) {
        this.idenImgPerson = idenImgPerson;
    }
}

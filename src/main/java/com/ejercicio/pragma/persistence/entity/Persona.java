package com.ejercicio.pragma.persistence.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "persona")
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer idPersona;
    private String nombre;
    private String apellido;
    @Column(name = "tipo")
    private Integer tipoId;
    @Column(name = "nro_Identificacion")
    private Integer nroIdentificacion;
    private Integer edad;
    @Column(name = "ciudad_nacimiento")
    private String ciudad;

    @Column(name = "id_foto")
    private String idImg;

    @OneToMany(mappedBy = "imgPersona",  cascade=CascadeType.ALL)
    private List<ImgPersona> imgsPersona;


    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getTipoId() {
        return tipoId;
    }

    public void setTipoId(Integer tipoId) {
        this.tipoId = tipoId;
    }

    public Integer getNroIdentificacion() {
        return nroIdentificacion;
    }

    public void setNroIdentificacion(Integer nroIdentificacion) {
        this.nroIdentificacion = nroIdentificacion;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

   public String getIdImg() {
        return idImg;
    }

    public void setIdImg(String idImg) {
        this.idImg = idImg;
    }

    public List<ImgPersona> getImgsPersona() {
        return imgsPersona;
    }

    public void setImgsPersona(List<ImgPersona> imgsPersona) {
        this.imgsPersona = imgsPersona;
    }
}

package com.ejercicio.pragma.persistence.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Imagen")
public class ImgPersona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer idImgPersona;
    @Column(name = "imagen")
    private String nomImgPersona;

    @Column(name = "ident_imag_persona")
    private Integer idenImgPersona;

    @ManyToOne
    @JoinColumn(name = "ident_imag_persona", insertable = false, updatable = false)
    private Persona imgPersona;


    public Integer getIdImgPersona() {
        return idImgPersona;
    }

    public void setIdImgPersona(Integer idImgPersona) {
        this.idImgPersona = idImgPersona;
    }

    public String getNomImgPersona() {
        return nomImgPersona;
    }

    public void setNomImgPersona(String nomImgPersona) {
        this.nomImgPersona = nomImgPersona;
    }

    public Persona getImgPersona() {
        return imgPersona;
    }

    public void setImgPersona(Persona imgPersona) {
        this.imgPersona = imgPersona;
    }

    public Integer getIdenImgPersona() {
        return idenImgPersona;
    }

    public void setIdenImgPersona(Integer idenImgPersona) {
        this.idenImgPersona = idenImgPersona;
    }
}

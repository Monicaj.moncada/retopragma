package com.ejercicio.pragma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjercicioPragma {

	public static void main(String[] args) {
		SpringApplication.run(EjercicioPragma.class, args);
	}

}

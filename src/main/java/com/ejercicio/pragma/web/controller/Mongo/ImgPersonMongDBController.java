package com.ejercicio.pragma.web.controller.Mongo;

import com.ejercicio.pragma.domain.dto.mongodb.ImaPersonMongoDtoDb;
import com.ejercicio.pragma.domain.service.Mongo.ImgPersonaMongoDBService;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/Imagenes")
public class ImgPersonMongDBController {

    Logger logger = Logger.getLogger("MyLog");
    @Autowired
    private ImgPersonaMongoDBService imgPersonaMongoDBService;

    @GetMapping("/AllImg")
    public ResponseEntity<List<ImaPersonMongoDtoDb>> getAllImg(){
        return new ResponseEntity<>(imgPersonaMongoDBService.getAllImg(),HttpStatus.OK);
    }

    @GetMapping("/Cedula/{id}")
    public ResponseEntity<List<ImaPersonMongoDtoDb>> consultarImagenPorIdenti(@PathVariable("id") int idenImgPersonD) {
        logger.info(" idenImgPersonD "+ idenImgPersonD);
        return new ResponseEntity<>(imgPersonaMongoDBService.consultarImagenPorIdenti(idenImgPersonD), HttpStatus.OK);
    }


    @PostMapping("/save")
    public ResponseEntity<ImaPersonMongoDtoDb> save(@RequestParam("idenImgPersonD") Integer idenImgPersonD, @RequestParam("archivo") MultipartFile archivo) throws IOException {
        ImaPersonMongoDtoDb imgPers = new ImaPersonMongoDtoDb();
        imgPers.setIdenImgPersonD(idenImgPersonD);
        Binary binary = new Binary(BsonBinarySubType.BINARY, archivo.getBytes());
        imgPers.setNameImgD(binary);
        imgPers.setIdD(idenImgPersonD.toString()+archivo.getOriginalFilename());
        imgPers = imgPersonaMongoDBService.save(imgPers);
        return new ResponseEntity<>(imgPers, HttpStatus.CREATED);
    }

    @DeleteMapping("")
    public ResponseEntity delete(@PathVariable ("id") int idenPerson){
        if(imgPersonaMongoDBService.delete(idenPerson)){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }


}

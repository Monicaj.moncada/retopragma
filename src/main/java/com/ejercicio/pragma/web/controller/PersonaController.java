package com.ejercicio.pragma.web.controller;

import com.ejercicio.pragma.domain.dto.PersonDto;
import com.ejercicio.pragma.domain.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/persons")
public class PersonaController {
    @Autowired
    private PersonaService personaService;

    @GetMapping("/all")
    public ResponseEntity<List<PersonDto>> getAll(){
        return new ResponseEntity<>(personaService.getAll(), HttpStatus.OK) ;
    }

    @GetMapping("/Id/{id}")
    public ResponseEntity<PersonDto> getPersona(@PathVariable("id") int idPersona){
        return personaService.getPerson(idPersona).
                map(pers -> new ResponseEntity<>(pers, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/Cedula/{id}")
    public  ResponseEntity<List<PersonDto>> getByPerson(@PathVariable("id") int idPersona){
        return new ResponseEntity<>(personaService.getByPerson(idPersona), HttpStatus.OK);
    }

    @GetMapping("/edadMayorIgual/{edad}")
    public ResponseEntity<List<PersonDto>> getAgePersona(@PathVariable("edad") int edadPersona){
        return personaService.getAgePersona(edadPersona).map(pers-> new ResponseEntity<>(pers, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    @GetMapping("/edadMenorIgual/{edad}")
    public ResponseEntity<List<PersonDto>> getAgePersonaMenorIgual(@PathVariable("edad") int edadPersona){
        return personaService.getAgePersonaMenorIgual(edadPersona).map(pers-> new ResponseEntity<>(pers, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/save")
    public ResponseEntity<PersonDto> save (@RequestBody PersonDto person){
        return new ResponseEntity<>(personaService.save(person), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete (@PathVariable("id") int personaId){
        if(personaService.delete(personaId)) {
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

}
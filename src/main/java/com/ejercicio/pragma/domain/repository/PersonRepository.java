package com.ejercicio.pragma.domain.repository;

import com.ejercicio.pragma.domain.dto.PersonDto;


import java.util.List;
import java.util.Optional;

public interface PersonRepository {
    List<PersonDto> getAll();
    List<PersonDto> getByPerson(int idPersona);
    Optional<PersonDto> getPersona(int idPersona);
    Optional<List<PersonDto>> getAgePersona(int edadPersona);
    Optional<List<PersonDto>> getAgePersonaMenorIgual(int edadPersona);
    PersonDto save(PersonDto persona);
    void delete(int idPersona);
}
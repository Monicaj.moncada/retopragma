package com.ejercicio.pragma.domain.repository.Mongo;

import com.ejercicio.pragma.domain.dto.mongodb.ImaPersonMongoDtoDb;

import java.util.List;
import java.util.Optional;

public interface ImgPersonRepositoryMongoDB {
    ImaPersonMongoDtoDb save(ImaPersonMongoDtoDb imgMongo);

    List<ImaPersonMongoDtoDb> getAllImg();

   List<ImaPersonMongoDtoDb> consultarImagenPorIdenti(int identificacion);

   void delete(int IdenPersona);
}

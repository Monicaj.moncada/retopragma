package com.ejercicio.pragma.domain.service.Mongo;

import com.ejercicio.pragma.domain.dto.mongodb.ImaPersonMongoDtoDb;
import com.ejercicio.pragma.domain.repository.Mongo.ImgPersonRepositoryMongoDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ImgPersonaMongoDBService {
    @Autowired
    private ImgPersonRepositoryMongoDB imgPersonRepository;

    public List<ImaPersonMongoDtoDb> getAllImg(){
        return imgPersonRepository.getAllImg();
    }

    public List<ImaPersonMongoDtoDb> consultarImagenPorIdenti(int identificacion){
        return imgPersonRepository.consultarImagenPorIdenti(identificacion);
    }

    public ImaPersonMongoDtoDb save (ImaPersonMongoDtoDb imgMongo){
        return imgPersonRepository.save(imgMongo);
    }

    public boolean delete(int identPersona){
       return false;
    }

}

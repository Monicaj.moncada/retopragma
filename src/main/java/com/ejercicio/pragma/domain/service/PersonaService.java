package com.ejercicio.pragma.domain.service;

import com.ejercicio.pragma.domain.dto.PersonDto;
import com.ejercicio.pragma.domain.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonaService {
    @Autowired
    private PersonRepository personRepository;

    public List<PersonDto> getAll(){
        return personRepository.getAll();
    }

    public Optional<PersonDto> getPerson(int personId ){
        return personRepository.getPersona(personId);
    }

    public Optional<List<PersonDto>> getAgePersona(int edadPersona){
        return personRepository.getAgePersona(edadPersona);
    }
    public Optional<List<PersonDto>> getAgePersonaMenorIgual(int edadPersona){
        return personRepository.getAgePersonaMenorIgual(edadPersona);
    }

    public List<PersonDto> getByPerson(int idPersona){
        return personRepository.getByPerson(idPersona);
    }

    public PersonDto save(PersonDto personDto){
        return personRepository.save(personDto);
    }

    public boolean delete (int personaId){
        return getPerson(personaId).map(pers -> {
            personRepository.delete(personaId);
            return true;
        }).orElse(false);
    }

}

package com.ejercicio.pragma.domain.dto;

public class ImgPersonDto {
    private Integer idImgPerson;
    private String nomImagPerson;

    private Integer idenImgPerson;

    public Integer getIdImgPerson() {
        return idImgPerson;
    }

    public void setIdImgPerson(Integer idImgPerson) {
        this.idImgPerson = idImgPerson;
    }

    public String getNomImagPerson() {
        return nomImagPerson;
    }

    public void setNomImagPerson(String nomImagPerson) {
        this.nomImagPerson = nomImagPerson;
    }

    public Integer getIdenImgPerson() {
        return idenImgPerson;
    }

    public void setIdenImgPerson(Integer idenImgPerson) {
        this.idenImgPerson = idenImgPerson;
    }
}

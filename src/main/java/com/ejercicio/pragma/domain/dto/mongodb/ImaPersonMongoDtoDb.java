package com.ejercicio.pragma.domain.dto.mongodb;
import org.bson.types.Binary;

public class ImaPersonMongoDtoDb {

    private String idD;
    private Binary nameImgD;
    private Integer idenImgPersonD;

    public String getIdD() {
        return idD;
    }

    public void setIdD(String idD) {
        this.idD = idD;
    }

    public Binary getNameImgD() {
        return nameImgD;
    }

    public void setNameImgD(Binary nameImgD) {
        this.nameImgD = nameImgD;
    }

    public Integer getIdenImgPersonD() {
        return idenImgPersonD;
    }

    public void setIdenImgPersonD(Integer idenImgPersonD) {
        this.idenImgPersonD = idenImgPersonD;
    }
}

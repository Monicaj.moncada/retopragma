package com.ejercicio.pragma.domain.dto;



public class PersonDto {
    private Integer idPerson;
    private String nombreP;
    private String apellidoP;
    private Integer tipoIdP;
    private Integer nroIdentificacionP;

    private Integer edadP;
    private String ciudadP;
    private String idImgP;

    public Integer getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Integer idPerson) {
        this.idPerson = idPerson;
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public String getApellidoP() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    public Integer getTipoIdP() {
        return tipoIdP;
    }

    public void setTipoIdP(Integer tipoIdP) {
        this.tipoIdP = tipoIdP;
    }

    public Integer getNroIdentificacionP() {
        return nroIdentificacionP;
    }

    public void setNroIdentificacionP(Integer nroIdentificacionP) {
        this.nroIdentificacionP = nroIdentificacionP;
    }

    public Integer getEdadP() {
        return edadP;
    }

    public void setEdadP(Integer edadP) {
        this.edadP = edadP;
    }

    public String getCiudadP() {
        return ciudadP;
    }

    public void setCiudadP(String ciudadP) {
        this.ciudadP = ciudadP;
    }

    public String getIdImgP() {
        return idImgP;
    }

    public void setIdImgP(String idImgP) {
        this.idImgP = idImgP;
    }

   }
